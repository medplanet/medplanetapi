<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product\Models\Product;
use App\Models\Product_category\Models\Product_category;
use App\Models\Product_description\Models\Product_description;
use App\Models\Product_halal\Models\Product_halal;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\RequestException;

class ProductController extends Controller {

    public function index() {

        $url = "https://api.alam247.xyz/getProducts";
        $output = curlAPI("GET", $url, false);
        $response = ($output);
        $data = json_decode($response);
//        dd($data);
        $product_halal = $data->product_data;
        $value = array();
        $index = 0;
        foreach ($product_halal as $i) {
            $product_id = $product_halal[$index]->id;
            $value[] = $product_id;
            $product_name = $product_halal[$index]->name;
            $product_price = $product_halal[$index]->price;
            $product_quantity = $product_halal[$index]->quantity;
            $product_capacity = $product_halal[$index]->capacity;
            $product_company_name = $product_halal[$index]->company_name;
            $product_type = $product_halal[$index]->type;
            $product_classification = $product_halal[$index]->classification;
            $product_generic_name = $product_halal[$index]->generic_name;
            $product_sale_price = $product_halal[$index]->sale_price;
            $product_purchase_price = $product_halal[$index]->purchase_price;
            $product_product_code = $product_halal[$index]->product_code;
            $product_date_mfg = $product_halal[$index]->date_mfg;
            $product_date_exp = $product_halal[$index]->date_exp;
            $product_product_description = $product_halal[$index]->product_description;
            $product_image = $product_halal[$index]->product_image;
            $product_adv_effect = $product_halal[$index]->adv_effect;
            $product_warning = $product_halal[$index]->warning;
            $manufacture_id = $product_halal[$index]->company_name;
            $slug = strtolower($product_name);
            $product_slug = str_replace(' ', '', $slug);
        

            $product = Product::where('products_id', $product_id)->first();


            if ($product) {
                $id = $product->products_id;
                $product_update = array('products_quantity' => $product_quantity,
                    'products_capacity' => $product_capacity, 'products_classification' => $product_classification,
                    'generic_name' => $product_generic_name,
                    'adv_effect' => $product_adv_effect, 'warning' => $product_warning,
                    'products_image' => $product_image, 'products_price' => $product_price,
                    'products_sale_price' => $product_sale_price, 'products_purchase_price' => $product_purchase_price,
                    'manufacturers_id' => $manufacture_id, 'products_slug' => $product_slug, 'is_shifted' => 1);
                DB::table('products')->where('products_id', $id)->update($product_update);


                $product_description_update = array('products_id' => $product_id,
                    'language_id' => 1, 'products_name' => $product_name,
                    'products_description' => $product_product_description, 'is_shifted' => 1);
                DB::table('products_description')->where('products_id', $id)->update($product_description_update);

                $product_category_update = array('categories_id' => $product_type, 'is_shifted' => 1);
                DB::table('products_to_categories')->where('products_id', $id)->update($product_category_update);
           exit;
                } else {

                $products = new Product();
                $products->products_id = $product_id;
                $products->products_quantity = $product_quantity;
                $products->products_capacity = $product_capacity;
                $products->products_classification = $product_classification;
                $products->generic_name = $product_generic_name;
                $products->products_code = $product_product_code;
                $products->adv_effect = $product_adv_effect;
                $products->warning = $product_warning;
                $products->products_image = $product_image;
                $products->products_price = $product_price;
                $products->products_sale_price = $product_sale_price;
                $products->products_purchase_price = $product_purchase_price;
                $products->products_slug = $product_slug;
                $products->manufacturers_id = $manufacture_id;
                $products->products_min_order = 1;
                $products->products_max_stock = 30;
                $products->is_shifted = 1;
                $products->save();


                $product_description = new Product_description();
                $product_description->products_id = $product_id;
                $product_description->language_id = 1;
                $product_description->products_name = $product_name;
                $product_description->products_description = $product_product_description;
                $product_description->is_shifted = 1;
                $product_description->save();

                $product_category = new Product_category();
                $product_category->products_id = $product_id;
                $product_category->categories_id = $product_type;
                $product_category->is_shifted = 1;
                $product_category->save();
                exit;
            }

            $index++;
        }
        $data = json_encode($value);
        $url = "https://api.alam247.xyz/updateProducts";
        $output = curlAPI("POST", $url, $data);
        $response = ($output);
//       dd($response);

        $status = json_encode(['data' => "Data Save successfully"], true);
        return response($status);
    }

}

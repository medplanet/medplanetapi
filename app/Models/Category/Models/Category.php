<?php

namespace App\Models\Category\Models;


use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    
     protected $table = 'categories';
    protected $fillable = [
        'categories_image','categories_icon','parent_id','sort_order','date_added','last_modified','categories_slug','categories_status','is_shifted'
    ];
}

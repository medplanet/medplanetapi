<?php

namespace App\Models\Image_halal\Models;


use Illuminate\Database\Eloquent\Model;

class Image_halal extends Model
{
    
     protected $table = 'uploads';
    protected $fillable = [
        'name','path','extension','caption','user_id','hash','public',
        'is_shifted'
    ];
    
    
}

<?php

namespace App\Models\Product_description\Models;


use Illuminate\Database\Eloquent\Model;

class Product_description extends Model
{
    
     protected $table = 'products_description';
    protected $fillable = [
        'products_id','language_id','products_name','products_description','products_url','is_shifted'
    ];
}

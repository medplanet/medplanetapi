<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company_halal\Models\Company_halal;
use App\Models\Manufacture\Models\Manufacture;
use App\Models\Manufacture_info\Models\Manufacture_info;
use Illuminate\Support\Facades\DB;

class ManufactureController extends Controller {

    public function index() {
        $url = "https://api.alam247.xyz/getCompanies";
        $output = curlAPI("GET", $url, false);
        $response = ($output);
        $data = json_decode($response);
//        dd($data);
        $company_halal_datas = $data->company_data;
        $value = array();
        $index = 0;
        foreach ($company_halal_datas as $i) {
            $meanu_id = $company_halal_datas[$index]->id;
            $value[] = $meanu_id;
            $manu_name = $company_halal_datas[$index]->name;
            $manu_image = $company_halal_datas[$index]->company_logo;
            $menu_email = $company_halal_datas[$index]->email;
            $manu_company_no = $company_halal_datas[$index]->company_no;
            $manu_registration_no = $company_halal_datas[$index]->registration_no;
            $slug = strtolower($manu_name);
            $manu_slug = str_replace(' ', '', $slug);

            $manufacture = Manufacture::where('manufacturers_id', $meanu_id)->first();
            
            if ($manufacture) {
                $id = $manufacture->manufacturers_id;
                $info_manu = array('manufacturer_name' => $manu_name,
                    'manufacturer_image' => $manu_image, 'manufacturers_slug' => $manu_slug, 'email' => $menu_email, 'company_no' => $manu_company_no, 'registration_no' => $manu_registration_no, 'is_shifted' => 1);
                DB::table('manufacturers')->where('manufacturers_id', $id)->update($info_manu);


                $info_manu_info = array('languages_id' => 1,
                    'manufacturers_url' => null, 'is_shifted' => 1);
                DB::table('manufacturers_info')->where('manufacturers_id', $id)->update($info_manu_info);
            } else {

                $manufactures = new Manufacture();
                $manufactures->manufacturers_id = $meanu_id;
                $manufactures->manufacturer_name = $manu_name;
                $manufactures->manufacturer_image = $manu_image;
                $manufactures->manufacturers_slug = $manu_slug;
                $manufactures->email = $menu_email;
                $manufactures->company_no = $manu_company_no;
                $manufactures->registration_no = $manu_registration_no;
                $manufactures->is_shifted = 1;
                $manufactures->save();

                $manufactures_info = new Manufacture_info();
                $manufactures_info->manufacturers_id = $meanu_id;
                $manufactures_info->languages_id = 1;
                $manufactures_info->manufacturers_url = null;
                $manufactures_info->is_shifted = 1;
                $manufactures_info->save();
            }
            $index++;
        }
//        dd($value);
        $data = json_encode($value);
       
        $url = "https://api.alam247.xyz/updateCompanies";
        $output = curlAPI("POST", $url, $data);
        $response = (json_decode($output));
         
//        dd($response);

        $status = json_encode(['data' => "Data save successfully"], true);
        return response($status);
    }

}

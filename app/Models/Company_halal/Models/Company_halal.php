<?php

namespace App\Models\Company_halal\Models;


use Illuminate\Database\Eloquent\Model;

class Company_halal extends Model
{
    
     protected $table = 'companies_halal';
    protected $fillable = [
        'name','email','company_no','registration_no','is_shifted','company_logo'
    ];
}

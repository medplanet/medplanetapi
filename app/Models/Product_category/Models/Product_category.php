<?php

namespace App\Models\Product_category\Models;


use Illuminate\Database\Eloquent\Model;

class Product_category extends Model
{
    
     protected $table = 'products_to_categories';
    protected $fillable = [
        'products_id','categories_id','is_shifted'
    ];
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Image_halal\Models\Image_halal;
use App\Models\Image\Models\Image;
use App\Models\Image_category\Models\Image_category;
use Illuminate\Support\Facades\DB;

class ImageController extends Controller {

    public function index() {
       $url = "https://api.alam247.xyz/Images";
        $output = curlAPI("GET", $url, false);
        $response = ($output);
        $data = json_decode($response);
//        dd($data);
         $image_datas = $data->category;
         $value = array();
        $index = 0;
        foreach ($image_datas as $i) {
            $image_id = $image_datas[$index]->id;
            $value[] = $image_id;
            $image_name = $image_datas[$index]->name;
            $path = $image_datas[$index]->path;
            $image_extension = $image_datas[$index]->extension;
            $image_caption = $image_datas[$index]->caption;
            $image_user_id = $image_datas[$index]->user_id;
            $image_hash = $image_datas[$index]->hash;
            $image_public = $image_datas[$index]->public;
           
//            $image_slug = str_replace(' ', '%20', $image_name);
//            $path = $image_hash .'/'. $image_slug ; 
//           echo $path;exit;
            $Image = Image::where('shifted_id', $image_id)->first();


            if ($Image) {
                $id = $Image->id;
                $image_update = array('name' => $image_name,
                    'user_id' => $image_user_id, 'is_shifted' => 1);
                DB::table('images')->where('id', $id)->update($image_update);

                $image_category_update = array('image_id' => $image_id,
                    'path' => $path, 'is_shifted' => 1);
                DB::table('image_categories')->where('image_id', $image_id)->where('is_shifted', '1')->update($image_category_update);
            } else {

                $image = new Image();
                $image->shifted_id = $image_id;
                $image->name = $image_name;
                $image->user_id = $image_user_id;
                $image->is_shifted = 1;
                $image->save();

                $image_category = new Image_category();
                $image_category->image_id = $image_id;
//            $image_category->image_type= ACTUAL;
                $image_category->height = 180;
                $image_category->width = 90;
                $image_category->path = $path;
                $image_category->is_shifted = 1;
                $image_category->save();
            }

            $index++;
        }
         $data = json_encode($value);
       
        $url = "https://api.alam247.xyz/updateImages";
        $output = curlAPI("POST", $url, $data);
        $response = (json_decode($output));
         
//        dd($response);
        $status = json_encode(['data' => "Data Save Successfully"], true);
        return response($status);
    }

}

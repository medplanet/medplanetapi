<?php

namespace App\Models\Manufacture_info\Models;


use Illuminate\Database\Eloquent\Model;

class Manufacture_info extends Model
{
    
     protected $table = 'manufacturers_info';
    protected $fillable = [
        'manufacturers_id','languages_id','manufacturers_url','is_shifted'
    ];
}

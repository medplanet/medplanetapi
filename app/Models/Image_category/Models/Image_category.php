<?php

namespace App\Models\Image_category\Models;


use Illuminate\Database\Eloquent\Model;

class Image_category extends Model
{
    
     protected $table = 'image_categories';
    protected $fillable = [
        'image_id','image_type','height','width','path','is_shifted'
    ];
    
    
}

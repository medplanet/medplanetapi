<?php

namespace App\Models\Category_description\Models;


use Illuminate\Database\Eloquent\Model;

class Category_description extends Model
{
    
     protected $table = 'categories_description';
    protected $fillable = [
        'categories_id','language_id','categories_name','categories_description','is_shifted'
    ];
}

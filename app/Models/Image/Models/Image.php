<?php

namespace App\Models\Image\Models;


use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    
     protected $table = 'images';
    protected $fillable = [
        'id','shifted_id','name','user_id','is_shifted'
    ];
    
    
}

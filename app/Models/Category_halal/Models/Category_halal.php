<?php

namespace App\Models\Category_halal\Models;


use Illuminate\Database\Eloquent\Model;

class Category_halal extends Model
{
    
     protected $table = 'categories_halal';
    protected $fillable = [
        'name','is_shifted'
    ];
}

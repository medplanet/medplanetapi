<?php

namespace App\Models\Product_halal\Models;


use Illuminate\Database\Eloquent\Model;

class Product_halal extends Model
{
    
     protected $table = 'products_halal';
    protected $fillable = [
        'name','price','quantity','type','capacity','company_name','classification',
        'generic_name','sale_price','purchase_price','product_code','date_mfg','date_exp',
        'product_description','product_image','is_shifted','adv_effect','warning'
    ];
}

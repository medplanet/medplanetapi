<?php

namespace App\Models\Manufacture\Models;


use Illuminate\Database\Eloquent\Model;

class Manufacture extends Model
{
    
     protected $table = 'manufacturers';
    protected $fillable = [
        'manufacturers_id','manufacturer_name','manufacturer_image','manufacturers_slug','email','company_no','registration_no','date_added','last_modified','is_shifted'
    ];
}

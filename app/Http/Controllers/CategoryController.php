<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category_halal\Models\Category_halal;
use App\Models\Category\Models\Category;
use App\Models\Category_description\Models\Category_description;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller {

    public function index() {
        $url = "https://api.alam247.xyz/Categories";
        $output = curlAPI("GET", $url, false);
        $response = ($output);
        $data = json_decode($response);
//        dd($data);
        $category_halal_datas = $data->category;
        $value = array();
        $index = 0;
        foreach ($category_halal_datas as $i) {
            $cate_id = $category_halal_datas[$index]->id;
            $value[] = $cate_id;
            $name = $category_halal_datas[$index]->name;
            $categ_n = strtolower($name);
            $cate_name = ucwords($categ_n);
            $slug = strtolower($name);
            $cate_slug = str_replace(' ', '', $slug);
            
             
            $category = Category::where('categories_id', $cate_id)->first();


            if ($category) {
                $id = $category->categories_id;
                $info_category = array('categories_image' => null,
                    'categories_icon' => null,'categories_slug' => $cate_slug,'is_shifted' => 1);
                DB::table('categories')->where('categories_id', $id)->update($info_category);


                $info_category_desc = array('language_id' => 1,
                    'categories_name' => $cate_name, 'categories_description'=> null ,'is_shifted' => 1);
                DB::table('categories_description')->where('categories_id', $id)->update($info_category_desc);
            }else{

            $category = new Category();
            $category->categories_id = $cate_id;
            $category->categories_image = '';
            $category->categories_icon = '';
            $category->parent_id = 0;
            $category->sort_order = 0;
            $category->categories_slug = $cate_slug;
            $category->categories_status = 1;
            $category->is_shifted = 1;
            $category->save();

            $category_description = new Category_description();
            $category_description->categories_id = $cate_id;
            $category_description->language_id = 1;
            $category_description->categories_name = $cate_name;
            $category_description->categories_description = null;
            $category_description->is_shifted = 1;
            $category_description->save();
            
            }
            $index++;
        }

        $data = json_encode($value);
        $url = "https://api.alam247.xyz/updateCategories";
        $output = curlAPI("POST", $url, $data);
        $response = ($output);
//       dd($response);
        $status = json_encode(['data' => "Category Record Save Successfully"], true);
        return response($status);
    }

}

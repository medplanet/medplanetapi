<?php

namespace App\Models\Product\Models;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
     protected $table = 'products';
    protected $fillable = [
        'products_id','products_quantity','products_capacity','products_classification','generic_name','products_code',
        'adv_effect','warning','products_model','products_image','products_video_link','products_price','products_sale_price',
        'products_purchase_price','products_date_added','products_last_modified','products_date_available','products_weight',
        'products_weight_unit','products_status','is_current','products_tax_class_id','manufacturers_id','products_ordered',
        'products_liked','low_limit','products_slug','products_min_order','products_max_stock','is_shifted'
    ];
    
    
}
